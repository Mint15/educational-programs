import java.util.function.BinaryOperator;

public class Calculator {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        System.out.println(calculator.calculate(10, 2, Integer::sum));
        System.out.println(calculator.calculate(10, 2, (a, b) -> a - b));
        System.out.println(calculator.calculate(10, 2, (a, b) -> a * b));
        System.out.println(calculator.calculate(10, 2, (a, b) -> a / b));
        System.out.println(calculator.calculate(10, 3, (a, b) -> a % b));
    }

    Integer calculate(Integer a, Integer b, BinaryOperator<Integer> c){
        return c.apply(a, b);
    }
}
