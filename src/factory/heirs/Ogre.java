package factory.heirs;

import factory.ann.DoRec;
import factory.ann.Random;
import factory.ann.NameGenerate;
import factory.Unit;

public class Ogre {

    @NameGenerate
    String name;
    @Random()
    int power;
    @DoRec
    Unit subordinate;

    @Override
    public String toString() {
        return "Ogre{" +
                "name='" + name + '\'' +
                ", power=" + power +
                ", subordinate=" + subordinate +
                '}';
    }

    public void setCoolName(){
        int a = (int)(Math.random() * 10);
        switch (a){
            case (0) :
                this.name = "Maestro";
                break;
            case (1):
                this.name = "Akakii";
                break;
            case (2):
                this.name = "Andrey";
                break;
            case (3):
                this.name = "Arbakaite";
                break;
            case (4):
                this.name = "Alina";
                break;
            case (5):
                this.name = "Armen";
                break;
            case (6):
                this.name = "Supercat";
                break;
            case (7):
                this.name = "Suleiman";
                break;
            case (8):
                this.name = "Ibragim";
                break;
            case (9):
                this.name = "Hurrem";
                break;
        }
    }
}
