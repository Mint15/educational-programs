package factory.heirs;

import factory.ann.DoRec;
import factory.ann.Random;
import factory.ann.NameGenerate;
import factory.Unit;

public class Goblin extends Unit {

    @NameGenerate
    String name;
    @Random()
    int power;
    @DoRec
    Unit subordinate;

    @Override
    public String toString() {
        return "Goblin{" +
                "name='" + name + '\'' +
                ", power=" + power +
                ", subordinate=" + subordinate +
                '}';
    }

    public void setCoolName(){
        int a = (int)(Math.random() * 10);
        switch (a){
            case (0) :
                this.name = "Velma";
                break;
            case (1):
                this.name = "Daphne";
                break;
            case (2):
                this.name = "Shaggy";
                break;
            case (3):
                this.name = "Scooby";
                break;
            case (4):
                this.name = "Fred";
                break;
            case (5):
                this.name = "Din";
                break;
            case (6):
                this.name = "Sam";
                break;
            case (7):
                this.name = "Dodo";
                break;
            case (8):
                this.name = "Patric";
                break;
            case (9):
                this.name = "SpongeBob";
                break;
        }
    }
}
