package factory.heirs;

import factory.ann.DoRec;
import factory.ann.GenUnitList;
import factory.ann.Random;
import factory.ann.NameGenerate;
import factory.Unit;

import java.util.List;

public class BattleGnome extends Unit {


    @NameGenerate()
    String name;
    @Random()
    int power;
    @DoRec
    Unit subordinate;
    @GenUnitList(count = 3, unitType = "factory.heirs.Goblin")
    List<Unit> unitList;

    public void setCoolName(){
        int a = (int)(Math.random() * 10);
        switch (a){
            case (0) :
                this.name = "Pucca";
                break;
            case (1):
                this.name = "Jetix";
                break;
            case (2):
                this.name = "Untitled";
                break;
            case (3):
                this.name = "Tutenstein";
                break;
            case (4):
                this.name = "Kuzko";
                break;
            case (5):
                this.name = "Kim 5+";
                break;
            case (6):
                this.name = "Baskov";
                break;
            case (7):
                this.name = "Fillip";
                break;
            case (8):
                this.name = "Kirkorov";
                break;
            case (9):
                this.name = "Hochydomoi";
                break;
        }
    }


    @Override
    public String toString() {
        return "BattleGnome{" +
                "name='" + name + '\'' +
                ", power=" + power +
                ", subordinate=" + subordinate +
                ", unitList= //" + unitList +
                "//}";
    }
}
