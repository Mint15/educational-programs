package factory;

import factory.ann.DoRec;
import factory.ann.GenUnitList;
import factory.ann.Random;
import factory.ann.NameGenerate;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Factory {
    public Unit createUnitByName(String unitClass) throws ClassNotFoundException, NoSuchMethodException,
            IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> aClass = Class.forName(unitClass);
        if (aClass.getSuperclass() == Unit.class) {
            Constructor<?> constructor = aClass.getConstructor();
            Unit newInstance = (Unit) constructor.newInstance();
            Arrays.stream(aClass.getDeclaredFields())
                    .forEach(field -> {
                        field.setAccessible(true);
                        if (field.isAnnotationPresent(DoRec.class)) {
                            try {
                                doRec(newInstance, field);
                            } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException
                                    | InvocationTargetException e) {
                                e.printStackTrace();
                            }
                        }
                        if (field.isAnnotationPresent(Random.class)){
                            doRandom(newInstance, field);
                        }
                        if (field.isAnnotationPresent(NameGenerate.class)){
                            try {
                                nameGenerate(newInstance, field);
                            } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
                                e.printStackTrace();
                            }
                        }
                        if (field.isAnnotationPresent(GenUnitList.class)){
                            if (((int)(Math.random() * 10) % 2) == 0) {
                                try {
                                    genUnitList(newInstance, field);
                                } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        field.setAccessible(false);
                    });
            return newInstance;
        }

        return null;
    }

    private void genUnitList(Unit newInstance, Field field) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, InvocationTargetException {
        GenUnitList ann = field.getAnnotation(GenUnitList.class);
        List<Unit> unitList = createUnitsByName(ann.unitType(), ann.count());
        field.set(newInstance, unitList);
    }

    public List<Unit> createUnitsByName(String unitClass, int count) throws ClassNotFoundException, InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        List<Unit> unitList = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            unitList.add(createUnitByName(unitClass));
        }
        return unitList;
    }



    public void nameGenerate(Object o, Field field) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        o.getClass().getMethod(field.getAnnotation(NameGenerate.class).method(), null)
                                .invoke(o, null);
    }



    private void doRec(Object o, Field field) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, InvocationTargetException {
                    try {
                        java.util.Random random = new java.util.Random();
                        if (random.nextInt(2) == 1) {
                            field.set(o, createUnitByName(o.getClass().getName()));
                        }
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
    }

    public void doRandom(Object o, Field field) {
                    Random ann = field.getAnnotation(Random.class);
                    int b = ann.max() - ann.min();
                    int a = (int) (Math.random() * ++b) + ann.min();
                    try {
                        field.set(o, a);
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
    }
}
