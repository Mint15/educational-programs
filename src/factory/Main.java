package factory;

import factory.heirs.*;

import java.lang.reflect.InvocationTargetException;

public class Main {

    public static void main(String[] args) {
        Factory factory = new Factory();
        /*try {
            System.out.println(factory.createUnitsByName("factory.heirs.BattleGnome", 3));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

         */
       try {
            try {
                System.out.println(factory.createUnitByName("factory.heirs.BattleGnome"));
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}
