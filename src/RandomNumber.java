import java.util.ArrayList;
import java.util.function.Supplier;

public class RandomNumber {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(0);
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.add(6);
        list.add(7);
        list.add(8);
        list.add(9);

        Supplier<Integer> getRandomNum = () ->{
            int value = (int)(Math.random() * list.size());
            return list.get(value);
        };

        for (int i = 0; i < 10; i++) {
            System.out.println(getRandomNum.get());
        }

    }
}
