package reflection;

public class Dog extends Mammal{

    protected Dog(String sex) {
        super(sex);
    }

    @Override
    void sleep(int hours) {

    }

    @Override
    public void move() {

    }
}
