package reflection;

public abstract class Mammal implements Animal{

    protected String sex;

    protected Mammal(String sex) {
        this.sex = "female";
    }

    abstract void sleep(int hours);
}
