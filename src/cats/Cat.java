package cats;

public class Cat {
    String name;
    int age;
    Cat fatherCat;
    Cat motherCat;
    String color;
    boolean isAlive;

    public Cat(String name, int age, Cat fatherCat, Cat motherCat, String color, boolean isAlive) {
        this.name = name;
        this.age = age;
        this.fatherCat = fatherCat;
        this.motherCat = motherCat;
        this.color = color;
        this.isAlive = isAlive;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Cat getFatherCat() {
        return fatherCat;
    }

    public Cat getMotherCat() {
        return motherCat;
    }

    public String getColor() {
        return color;
    }

    public boolean isAlive() {
        return isAlive;
    }
}
