package cats;

import java.util.*;
import java.util.stream.Collectors;

public class CatManager {

    private Cat[] sourceArray;


    public Map<String, Cat> getNameCatMap() {
        TreeMap<String, Cat> nameCatMap = new TreeMap<>();
        for (Cat cat : sourceArray) {
            String name = cat.getName();
            nameCatMap.put(name, cat);
        }
        return nameCatMap;
    }

    public Map<String, Cat> getNameCatMapStream(){
        return Arrays.stream(sourceArray)
                .collect(Collectors.toMap(Cat::getName, cat -> cat));
    }

    public Map<Integer, List<Cat>> getAgeCatMap() {
        TreeMap<Integer, List<Cat>> ageCatMap = new TreeMap<>();
        for (Cat cat : sourceArray) {
            int age = cat.getAge();
            List<Cat> currentCats = ageCatMap.get(age);
            if (currentCats == null) {
                currentCats = new LinkedList<>();
            }
            currentCats.add(cat);
            ageCatMap.put(age, currentCats);
        }
        return ageCatMap;
    }

    public Map<Integer, List<Cat>> getAgeCatMapStream() {
        return Arrays.stream(sourceArray)
                .collect(Collectors.groupingBy(cat -> cat.getAge()));
                /*.collect(Collectors.toMap(Cat::getAge, cat -> Arrays.stream(sourceArray)
                        .filter(cat1 -> cat1.getAge() == cat.getAge())
                        .collect(Collectors.toList())));

                 */
    }

    public Map<Cat, List<Cat>> getChildrenCatMap() {
        HashMap<Cat, List<Cat>> childrenCatMap = new HashMap<>();
        for (Cat cat : sourceArray) {
            Cat mother = cat.getMotherCat();
            Cat father = cat.getFatherCat();

            List<Cat> motherCats = childrenCatMap.get(mother);
            if (motherCats == null) {
                motherCats = new LinkedList<>();
            }
            motherCats.add(cat);
            childrenCatMap.put(mother, motherCats);

            List<Cat> fatherCats = childrenCatMap.get(father);
            if (fatherCats == null) {
                fatherCats = new LinkedList<>();
            }
            fatherCats.add(cat);
            childrenCatMap.put(father, fatherCats);
        }
        return childrenCatMap;
    }

    public Map<Cat, List<Cat>> getChildrenCatMapStream(ArrayList<Cat> cats) {
        return cats.stream()
                .collect(Collectors.toMap(cat -> cat, cat -> cats.stream()
                        .filter(cat1 -> cat1.getMotherCat() == cat || cat1.getFatherCat() == cat)
                        .collect(Collectors.toList())));
    }

    public Map<Boolean, List<Cat>> getLiveStatusMap(){
        HashMap<Boolean, List<Cat>> liveStatusCatMap = new HashMap<>();
        List<Cat> aliveCats = new LinkedList<>();
        List<Cat> deadCats = new LinkedList<>();
        for (Cat cat : sourceArray){
            if (cat.isAlive){
                aliveCats.add(cat);
            } else {
                deadCats.add(cat);
            }
        }
        liveStatusCatMap.put(true, aliveCats);
        liveStatusCatMap.put(false, deadCats);
        return liveStatusCatMap;
    }

    public Map<Boolean, List<Cat>> getLiveStatusMapStream(ArrayList<Cat> cats){
        return cats.stream()
                .collect(Collectors.partitioningBy(Cat::isAlive));
                /*.collect(Collectors.toMap(Cat::isAlive, cat -> cats.stream()
                        .filter(cat1 -> cat1.isAlive() == cat.isAlive())
                        .collect(Collectors.toList())));

                 */
    }


    }
